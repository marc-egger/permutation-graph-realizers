#!/usr/bin/env sage


class AdjacencyTuple:
    
    def __init__(self, _origin, _vertex, _iClass=None, _inverseTuple=None):
        self.origin = _origin
        self.vertex = _vertex
        self.iClass = _iClass
        self.inverseTuple = _inverseTuple
           
    def __str__(self):
        return "t" + str((self.vertex, self.iClass))
        
    def __repr__(self):
        return str(self)

    def __int__(self):
        return self.vertex

    def __eq__(self,other):
        return other == int(self)

    def __ne__(self,other):
        return other != int(self)
    
    def __lt__(self,other):
        return int(self) < other
    
    def __le__(self,other):
        return int(self) <= other

    def __gt__(self,other):
        return int(self) > other

    def __ge__(self,other):
        return int(self) >= other


def extendedAdjacencyList(G):
    """ This function requires the vertex labels of G to be in
    range(len(G)).
    """
    n = len(G)
    assert G.vertices() == range(n)
    A = [[] for _ in xrange(n)]
    for a in xrange(n):
        for b in G.neighbors(a):

            adjTuple = AdjacencyTuple(b, a)
            A[b].append(adjTuple)

    # Add a link to the tuple (b,a) to the tuple (a,b)
    inversePointers = [0]*n 
    for a in xrange(n):
        for ab in A[a]:
            if a < ab:
                b = ab.vertex
                inverseTuple = A[b][inversePointers[b]]
                inversePointers[b] += 1
                ab.inverseTuple = inverseTuple
                inverseTuple.inverseTuple = ab

    return A


def iterInf(iterable, tail):
    """ Returns an iterator of "iterable" which returns "tail"
    instead of raising a StopIteration error after iterating
    through "iterable".
    """
    it = iter(iterable)
    for i in it:
        yield i
    while True:
        yield tail


def exploreImplicationClass((a,b),k , A):
    """ Input must be comparability graph.
    """
    n = len(A)
    iter_Aa = iterInf(A[a], infinity)
    iter_Ab = iterInf(A[b], infinity)
    ac = iter_Aa.next()
    bc = iter_Ab.next()
    while not ac == bc == infinity:
        if ac < bc:
            if ac.iClass == None:
                ac.iClass = k
                ac.inverseTuple.iClass = -k
                exploreImplicationClass((a,ac.vertex), k, A)
            ac = iter_Aa.next()
        elif bc < ac:
            if bc.iClass == None:
                bc.iClass = -k
                bc.inverseTuple.iClass = k
                exploreImplicationClass((bc.vertex, b), k, A)
            bc = iter_Ab.next()
        else:
            ac = iter_Aa.next()
            bc = iter_Ab.next()

    
def enumerateImplicationClasses(G):
    """ output:
    - A: Extended adjacency list with an index of an implication class
         assigned to each adjacency tuple.
    - k: Number of implication classes.
    """
    n = len(G)
    A = extendedAdjacencyList(G)  
    k = 0
    for a in xrange(n):
        for ab in A[a]:
            if ab.iClass == None:
                k += 1
                exploreImplicationClass((a,ab.vertex), k, A)
                
    return A, k


def iterIntersection(sortedIterable1, sortedIterable2):
    iter1 = iterInf(sortedIterable1, infinity)
    iter2 = iterInf(sortedIterable2, infinity)
    el1 = iter1.next()
    el2 = iter2.next()
    while not el1 == el2 == infinity:
        if el1 < el2:
            el1 = iter1.next()
        elif el2 < el1:
            el2 = iter2.next()
        else:
            yield el1, el2
            el1 = iter1.next()
            el2 = iter2.next()

            
def enumerateMaximalSimplex(bc, j, A, L):
    """ Enumerates the maximal simplex with index "j" containing the edge
        (b,c). (b,c) must be the lexicographically smallest edge whose
        implcation class does not yet belong to another maximal simplex.
        Also b<c must hold.
        output:
        - S_vertices: List containing the vertices of the simplex
        - S_iClasses: Adjacency matrix of simplex containing the indices
                      of the implication classes of the edges.
    """
    b = bc.origin
    c = bc.vertex
    k = bc.iClass
    S_vertices = [b,c]
    S_iClasses = [[ 0, k],
                  [-k, 0]]
    L[abs(k)] = j

    for ba, ca in iterIntersection(A[b], A[c]):
        ba_cClass = abs(ba.iClass)
        ca_cClass = abs(ca.iClass)
        notInMultiplex = L[ba_cClass] != j and L[ca_cClass] != j
        if ba_cClass != ca_cClass and notInMultiplex:
            S_iClasses.append([])
            a = ba.vertex
            i_ax = -1
            
            # Add new implication classes to adjacency matrix
            for i_x in xrange(len(S_vertices)):
                x = S_vertices[i_x]
                i_ax = A[a].index(x, i_ax+1)
                ax = A[a][i_ax]
                S_iClasses[-1].append(ax.iClass)
                S_iClasses[i_x].append(-ax.iClass)
                L[abs(ax.iClass)] = j
            S_iClasses[-1].append(0)
            
            S_vertices.append(a)
            
    return S_vertices, S_iClasses


def enumerate_all_maximal_simplexes(A, numberOfcClasses):
    """ output:
    - all_simpl_verts: List of the vertex lists of the simplexes.
    - all_simpl_iClasses: List of the adjacency matrices of the simplexes
                          which contain the indices of the implication
                          classes.
    - L: List which maps each implication class to the simplex to which it
         belongs.
    """
    L = [None]*(numberOfcClasses+1)
    all_simpl_verts = []
    all_simpl_iClasses = []
    j = -1
    for b in xrange(len(A)):
        for bc in A[b]:
            c = bc.vertex
            if b < c and L[abs(bc.iClass)] == None:
                j += 1
                S_vertices, S_iClasses = enumerateMaximalSimplex(bc, j,
                                                                 A, L)
                all_simpl_verts.append(S_vertices)
                all_simpl_iClasses.append(S_iClasses)
                
    return all_simpl_verts, all_simpl_iClasses, L


def iClasses_from_order(order, S_iClasses):
    """ Computes the list of indices of the implication classes which are
    chosen according to the given order of the simplex vertices.
    """
    iClasses = []
    for a in xrange(len(S_iClasses)):
        for b in xrange(a+1, len(S_iClasses)):
            if order[a] < order[b]:
                iClasses.append(S_iClasses[a][b])
            else:
                iClasses.append(-S_iClasses[a][b])
    return iClasses


def transitive_orientations_from_simplexes(all_simpl_verts,
                                           all_simpl_iClasses):
    """ Returns an iteraterator which iterates through all transitive
    orientations of the given simplexes.
    output:
    - iClasses_TO: List of indices of the implication classes present int
                   the transitive orientation.
    """  
    number_of_simplexes = len(all_simpl_verts)
    number_of_orders = [factorial(len(all_simpl_verts[j]))
                        for j in xrange(number_of_simplexes)]
    order_indices = [xrange(number_of_orders[j])
                     for j in xrange(number_of_simplexes)]
    order_combinations = cartesian_product_iterator(order_indices)
    for order_c in order_combinations:
        iClasses_TO = []
        for j in xrange(number_of_simplexes):
            S_size = len(all_simpl_verts[j])
            iClasses = iClasses_from_order(Permutations(S_size)[order_c[j]],
                                           all_simpl_iClasses[j])
            iClasses_TO.extend(iClasses)
        yield iClasses_TO


def iterate_all_transitive_orientations(G):
    """ Computes all transitive orientations of G.
    output:
    TOs: Iterator which iterates through the transitive orientations,
         represented by a list of the implication class contained in the
         transitive orientations.
    A: Extended adjacency list of G
    numberOfcClasses: Number of color classes of G
    """
    A, numberOfcClasses = enumerateImplicationClasses(G)
    _temp = enumerate_all_maximal_simplexes(A, numberOfcClasses)
    all_simpl_verts, all_simpl_iClasses, L = _temp

    ### Test whether the enumeration of the implication classes and
    ### simplixes is correct
    # test_A(A, numberOfcClasses, G)
    # test_simplexes(all_simpl_verts, all_simpl_iClasses, L, A ,
    #                numberOfcClasses, G)

    TOs = transitive_orientations_from_simplexes(all_simpl_verts,
                                                 all_simpl_iClasses)
    return TOs, A, numberOfcClasses


def TO_degrees(iClasses, A, numberOfcClasses):
    """ Compute the in- and out-degrees of the vertices in the transitive
    orientation which contains the given implication classes.
    """
    iClassChosen = [False for _ in xrange(-numberOfcClasses,
                                          numberOfcClasses+1)]
    for k in iClasses:
        iClassChosen[k] = True

    inDegrees = [0]*len(A)
    outDegrees = [0]*len(A)
    for A_a in A:
        for ab in A_a:
            if iClassChosen[ab.iClass]:
                inDegrees[ab.vertex] += 1
                outDegrees[ab.origin] += 1

    return inDegrees, outDegrees

    
def compute_all_realizers(G):
    """ output:
    - realizers: Set of realizers of G represented as tuples in one-line
      notation.
    """
    # Relabel the vertices of G such that they range from 0 to len(G)
    G.relabel(dict(zip(G.vertices(), xrange(len(G)))))

    TOs, A, numberOfcClasses = iterate_all_transitive_orientations(G)
    _temp = iterate_all_transitive_orientations(G.complement())
    TOs_c, A_c, numberOfcClasses_c = _temp
    TOs_c = list(TOs_c)
    realizers = set()
    for TO in TOs:
        for TO_c in TOs_c:
            inDeg, outDeg = TO_degrees(TO, A, numberOfcClasses)
            inDeg_c, outDeg_c = TO_degrees(TO_c, A_c, numberOfcClasses_c)

            inDeg_FFp = [inDeg[a]+inDeg_c[a] for a in xrange(len(inDeg))]
            inDeg_FcFp = [outDeg[a]+inDeg_c[a] for a in xrange(len(inDeg))]

            p = [None]*len(inDeg_FFp)
            for a in xrange(len(inDeg_FFp)):
                p[inDeg_FFp[a]] = inDeg_FcFp[a]+1

            realizers.add(tuple(p))

    return realizers


############ TESTS #############           

def test_A(A, numberOfcClasses, G):
    """ Tests:
    (1) Compare implication classes with slow but robust method
        using incidence matrix
    (2) pointer to inverse tuple "ba" is correct
    (3) iClass of "ab" == -iClass of "ba"
    (4) cClasses ranges from 1 to len(cClasses)
    """

    # Test (1)
    compare_A_to_incidenceGraph_method(A, numberOfcClasses, G)

    def getTuple(i,j):
        index = A[i].index(j)
        return A[i][index]

    cClasses = set()
    for a in xrange(len(G)):
        for b in G.neighbors(a):
            ab = getTuple(a,b)
            cClasses.add(abs(ab.iClass))

            ba = getTuple(b,a)
            # Test (2)
            assert ab.inverseTuple == ba, (a,b)
            # Test (3)
            assert ab.iClass == -ba.iClass, (a,b)

    # Test (4)
    assert cClasses == set(xrange(1, len(cClasses)+1)), cClasses

    print "test_A successful."
            

def enumerate_iClasses_incidenceGraph_method(G):
    """ First Compute incidence graph of the forcing relation.
    The connected components are the equivalence classes of the
    relation, i.e. the implication Classes.
    """
    K = Graph()
    for a in xrange(len(G)):
        for b in G.neighbors(a):
            K.add_vertex((a,b))

            for c in G.neighbors(a):
                if c != b and not G.has_edge(c,b):
                    K.add_edge((a,c), (a,b)) 
            
            for c in G.neighbors(b):
                if c != a and not G.has_edge(a,c):
                    K.add_edge((c,b), (a,b))
    return [set(cc) for cc in K.connected_components()]


def compare_A_to_incidenceGraph_method(A, numberOfcClasses, G):

    A_iClasses = [set() for _ in xrange(numberOfcClasses*2+1)]
    for i in xrange(len(A)):
        for jt in A[i]:
            j = jt.vertex
            A_iClasses[jt.iClass].add((i,j))
    A_iClasses.remove(set())

    K_iClasses = enumerate_iClasses_incidenceGraph_method(G)
    for Acl in A_iClasses:
        assert Acl in K_iClasses
    for Kcl in K_iClasses:
        assert Kcl in A_iClasses


def test_simplexes(all_simpl_verts, all_simpl_iClasses, L, A,
                   numberOfcClasses, G):
    """ Tests:
    (1): L containes index of simplex for each implication class
    (2): Every implication class is represented exactly once in some
         simplex
    (3): The infomration in L and S_iClasses coincides
    (4): S_iClasses is a square matrix
    (5): Implication classes in the matrix S_iClasses are correct and the
         edges exist in A.
    (6): Let S be a simplex. If a,b,c is a simplex and a,b are in S but c
         is not, then the color classes of (a,b) and (b,c) must be in S.
         Here we test whether S is maximal, i.e. it can't be extended by
         adding c.
    """
    def getTuple(i,j):
        index = A[i].index(j)
        return A[i][index]

    # Test (1)
    assert None not in L[1:]

    iClasses_in_simplexes = [k for k in flatten(all_simpl_iClasses)
                             if k!=0]

    # Test (2)
    target = [k for k in xrange(-numberOfcClasses,numberOfcClasses+1)
              if k!=0]
    assert sorted(iClasses_in_simplexes) == target

    for j in xrange(len(all_simpl_iClasses)):
        cClasses_from_L = [cClass for cClass in xrange(len(L))
                           if L[cClass]==j]
        
        S_iClasses = all_simpl_iClasses[j]
        S_vertices = all_simpl_verts[j]
        cClasses_from_S = [abs(k) for k in flatten(S_iClasses) if k > 0]
        
        # Test (3)
        assert sorted(cClasses_from_L) == sorted(cClasses_from_S)

        # Test (4)
        assert all(len(row)==len(S_iClasses) for row in S_iClasses)

        neighbors_intersection = set(G.vertices())
        for a_S in xrange(len(S_iClasses)):
            a = S_vertices[a_S]

            neighbors_intersection.intersection_update(G.neighbors(a))
            
            for b_S in xrange(len(S_iClasses)):
                if b_S != a_S:
                    b = S_vertices[b_S]

                    # Test (5)
                    ab = getTuple(a,b)
                    assert ab.iClass == S_iClasses[a_S][b_S]
                    ba = getTuple(b,a)
                    assert ba.iClass == S_iClasses[b_S][a_S]
                    
        a = S_vertices[0]
        b = S_vertices[1]
        ab = getTuple(a, b)
        for ac in A[a]:
            c = ac.vertex
            if c not in S_vertices and c in neighbors_intersection:
                bc = getTuple(b, c)
                if len(set([abs(ab.iClass),
                            abs(ac.iClass), abs(bc.iClass)])) == 3:
                    # Test (6)
                    assert abs(ac.iClass) in cClasses_from_S
                    assert abs(bc.iClass) in cClasses_from_S
                
    print "test_simplexes successful."

  
def main():
    P = Permutations(15).random_element()
    G = graphs.PermutationGraph(P)
    print compute_all_realizers(G)
       

if __name__ == "__main__":
    main()
else:
    from sage.all import *
