The code implements algorithms for computing all permutations which give rise 
to the same permutation graph (called realizers) as described in 

master-thesis-marc-egger.pdf

The code is also in Appendix A of the thesis and you may find some more
information about the algorithms there.

The programming language used is SageMath code. Head to http://www.sagemath.org
for more information.